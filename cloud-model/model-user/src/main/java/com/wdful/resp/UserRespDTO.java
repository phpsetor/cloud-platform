package com.wdful.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wdful.enums.Authenticate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel("用户基本信息返回")
public class UserRespDTO implements Serializable {
    private static final long serialVersionUID = 3771241945747099509L;

    @ApiModelProperty("用户token")
    private String token;

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("用户手机号")
    private String mobile;

    @ApiModelProperty(value = "上次登陆时间",example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private LocalDateTime lastLoginTime;

    @ApiModelProperty("登陆次数")
    private Integer loginAcc = 0;

    @ApiModelProperty("权限标识")
    private Authenticate authenticate;

    @ApiModelProperty("用户详细信息")
    private UserInfoRespDTO userInfo;

}
