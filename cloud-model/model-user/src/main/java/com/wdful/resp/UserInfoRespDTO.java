package com.wdful.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wdful.enums.Authenticate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel("用户详细信息返回")
public class UserInfoRespDTO implements Serializable {

    @ApiModelProperty("昵称")
    private String  nickName;

    @ApiModelProperty("头像")
    private String  headImg;

    @ApiModelProperty("简介")
    private String  profile;

    @ApiModelProperty("真实姓名")
    private String  trueName;

    @ApiModelProperty("是否实名")
    private Boolean isAuth;

    @ApiModelProperty("用户等级")
    private Integer level = 0;








}
