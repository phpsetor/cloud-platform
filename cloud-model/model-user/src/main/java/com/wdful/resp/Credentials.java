package com.wdful.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@ApiModel("临时秘钥")
public class Credentials {


    @ApiModelProperty("临时秘钥")
    private CredentialsBean credentials;
    @ApiModelProperty("请求id")
    private String requestId;
    @ApiModelProperty("过期时间")
    private String expiration;
    @ApiModelProperty("开始时间")
    private int startTime;
    @ApiModelProperty("过期时间戳")
    private int expiredTime;


    @NoArgsConstructor
    @Data
    public static class CredentialsBean {
        /**
         * tmpSecretId : AKIDDpoHNzLKIKlIdtb4FwsVlz9vF46gE4F8H2YDJwm5EK3YI3mEPbI98J14K9RpiDxI
         * tmpSecretKey : C8u/I6eb7ZuNHzptsXU6Z5WNJ64ItM7qh7nRwc6k89w=
         * sessionToken : vm5rnZmTko45jXKtPS1eI7BWrP4xy3Ca090345d9736193c42cce5ad93fd616cdrEk82cckpbCn_YJSwLvsIYGuJWhRWxJqRjIpb_8kkTEekufkiUxIAip667yXaCNs5IZMPtEGgHxuMJMS2FwyNyGmXdVx6HhYsdg_rL7XioP0VDTEwXl5qRNdkeEvF277HMYbw4rBVQyW8f3hT04_2s9k7dq32SBY52Qb7h6X1mmaHV471AyTBnDgEmdCX-1E4Ck77RpbulMbEqEx4Lzqrw-Y0TI9AchDXUn9Yk3lGYS1hHZ4O1n5pGReJCeU1QoUoaXrcmcKYjvAXIhTij62mhtd8ZlKt5ir2naE_ge2z-H15EvC3YEGMqpodQRH-bhqnXHcYlqEYIxYMfP_2XKl7ntQZ1gFFPekj0chFjJbKcUo-dx2RO1AknBwgC0P6HjlBpgwmgw5xgDGpRagdykzy_5JtquqMm-sONG2F5zLCoLo-Ojdd5JgVN7a-KvAo8PgzvBUSZKxtza-23oMo1XAB2k1rhphJMV46-V5iAf-Pd-e2e_I3zlZbibe2qG6FPTi38DxPvZB8yEIHUI9L3P_FhahqMlwk5DNObZ-lPS3c-xZzp2tpMbOPQhQSNWvpEiJ
         */

        @ApiModelProperty("临时秘钥id")
        private String tmpSecretId;
        @ApiModelProperty("临时秘钥key")
        private String tmpSecretKey;
        @ApiModelProperty("会话token")
        private String sessionToken;
    }
}
