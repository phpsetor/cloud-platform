package com.wdful.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@ApiModel("用户注册dto")
@Data
public class UserReqDTO implements Serializable {
    private static final long serialVersionUID = 466616145363031111L;

    @Pattern(regexp = "^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[0-9]))\\d{8})$", message = "手机号格式错误")
    @ApiModelProperty("手机号")
    private String mobile;

    @NotEmpty(message = "短信验证码不可为空")
    @ApiModelProperty("短信验证码")
    private String verifyCode;

//    @Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9]{8,18}$",message = "密码安全性太低")
    @ApiModelProperty("密码")
    private String password;

}
