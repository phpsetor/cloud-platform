package com.wdful.enums;

import java.io.Serializable;

/**
 * @author Lee
 * @description 枚举父类
 * @date 2020/3/16 17:13
 **/
public interface EnumerableValue extends Serializable {

    int getValue();
}
