package com.wdful.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Lee
 * @description 权限枚举
 * @date 2020/9/9 3:00 下午
 **/
@Getter
@AllArgsConstructor
public enum Authenticate implements EnumerableValue {
    USER(1,"个人用户"),
    STUDENT(2,"学生用户"),
    TEACHER(3,"教师用户"),
    MANAGER(4,"学校管理员"),
    COMPANY(5,"企业用户"),
    MASTER(6,"大师"),
    ;
    private int value;
    private String desc;

    public static class Converter extends BaseEnumValueConverter<Authenticate> {
    }

}
