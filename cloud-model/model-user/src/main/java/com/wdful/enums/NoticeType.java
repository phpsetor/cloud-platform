package com.wdful.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Lee
 * @description 消息模板类型
 * @date 2020/3/27 17:04
 **/
@Getter
@AllArgsConstructor
public enum NoticeType implements EnumerableValue {

    SMS_REGISTER(1, "", "register:code:", "注册短信"),
    SMS_LOGIN(2, "", "login:code:", "登陆短信"),
    SMS_RESET_PASSWORD(3, "", "resetpassword:code:", "重置密码"),
    SMS_CHANGE_PASSWORD(3, "", "changepassword:code:", "修改密码"),
    ;

    public static class Converter extends BaseEnumValueConverter<NoticeType> {
    }

    private int value;
    private String content;
    private String redisKey;
    private String desc;
}
