package com.wdful.domain;
import com.wdful.enums.NoticeType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;
import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "t_sms_record", indexes = {@Index(columnList = "create_time"), @Index(columnList = "resv_id")})
@Where(clause = "is_delete = 0")
public class SmsRecord extends BaseEntityIncre {

    @Column(name = "mobile", columnDefinition = "TEXT comment '接收手机号'")
    @Lob
    private String mobile;

    @Column(name = "resv_id", columnDefinition = "BIGINT(20) comment '接收人id'")
    private Long resvId;

    @Column(name = "notice_type", columnDefinition = "INT(2) comment '通知类型'")
    @Convert(converter = NoticeType.Converter.class)
    private NoticeType noticeType;

    @Column(name = "resp", columnDefinition = "TEXT comment '返回结果'")
    @Lob
    private String resp;

    @Column(name = "content", columnDefinition = "TEXT comment '发送内容'")
    @Lob
    private String content;
}
