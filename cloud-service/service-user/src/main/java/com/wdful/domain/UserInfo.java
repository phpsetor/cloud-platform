package com.wdful.domain;

import com.wdful.enums.Authenticate;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author Lee
 * @description 用户扩展信息表
 * @date 2020/9/2 2:59 下午
 **/
@Table(name = "t_user_info")
@Entity
@Getter
@Setter
@Where(clause = "is_delete = 0")
public class UserInfo extends BaseEntityIncre{

    @OneToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id",columnDefinition = "BIGINT(20) comment '关联用户id'")
    private User user;

    @Column(name = "nick_name",columnDefinition = "VARCHAR(55) comment '昵称'")
    private String  nickName;

    @Column(name = "head_img",columnDefinition = "VARCHAR(55) comment '头像'")
    private String  headImg;

    @Column(name = "live_province",columnDefinition = "VARCHAR(22) comment '省地址'")
    private String  liveProvince;

    @Column(name = "live_city",columnDefinition = "VARCHAR(22) comment '市地址'")
    private String  liveCity;

    @Column(name = "live_addr",columnDefinition = "VARCHAR(155) comment '详细地址'")
    private String  liveAddr;

    @Column(name = "profile",columnDefinition = "VARCHAR(255) comment '简介'")
    private String  profile;

    @Column(name = "true_name",columnDefinition = "VARCHAR(22) comment '真实姓名'")
    private String  trueName;

    @Column(name = "id_card_num",columnDefinition = "VARCHAR(18) comment '身份证号'")
    private String  idCardNum;

    @Column(name = "is_auth",columnDefinition = "BIT default 0 comment '是否实名'")
    private Boolean isAuth = false;

    @Column(name = "level",columnDefinition = "INT(4) default 0 comment '用户等级'")
    private Integer level = 0;

    @Column(name = "school_id",columnDefinition = "BIGINT(20) comment '学院id'")
    private Long schoolId;

    @Column(name = "profession_id",columnDefinition = "BIGINT(20) comment '专业id'")
    private Long professionId;


    @Column(name = "id_card_A",columnDefinition = "VARCHAR(255) comment '身份证正面'")
    private String  idCardA;

    @Column(name = "id_card_B",columnDefinition = "VARCHAR(255) comment '身份证背面'")
    private String  idCardB;

}
