package com.wdful.domain;

import com.wdful.enums.Authenticate;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Lee
 * @description 用户表
 * @date 2020/9/2 2:33 下午
 **/

@Table(name = "t_user")
@Entity
@Getter
@Setter
@Where(clause = "is_delete = 0")
public class User extends BaseEntityIncre {

    @Column(name = "mobile", columnDefinition = "VARCHAR(12) comment '手机号'")
    private String mobile;

    @Column(name = "password", columnDefinition = "VARCHAR(255) comment '密码'")
    private String password;

    @Column(name = "last_login_time", columnDefinition = "DATETIME comment '上次登陆时间'")
    private LocalDateTime lastLoginTime;

    @Column(name = "login_acc", columnDefinition = "INT(11) comment '登陆次数'")
    private Integer loginAcc = 0;

    @Column(name = "enabled", columnDefinition = "BIT(1) default 1 comment '是否启用'")
    private Boolean enabled;

    @Column(name = "last_login_ip", columnDefinition = "VARCHAR(55) comment '上次登陆IP'")
    private String lastLoginIp;

    @Column(name = "authenticate",columnDefinition = "INT(2) comment '认证'")
    @Convert(converter = Authenticate.Converter.class)
    private Authenticate authenticate = Authenticate.USER;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "user")
    @NotFound(action = NotFoundAction.IGNORE)
    private UserInfo userInfo;


}
