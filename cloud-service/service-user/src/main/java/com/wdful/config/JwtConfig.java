package com.wdful.config;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.security.Key;

@Component
public class JwtConfig {

    private Key key;

    @Value("${jwt.base64-secret}")
    public void initValues(String base64Secret) {
        byte[] keyBytes = Decoders.BASE64.decode(base64Secret);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    @Bean("jwtParser")
    public JwtParser genJwtParser() {
        final JwtParser build = Jwts.parserBuilder()
                .setSigningKey(key)
                .build();
        return build;
    }

    @Bean("jwtBuilder")
    public JwtBuilder genJwtBuilder() {
        final JwtBuilder build = Jwts.builder()
                .signWith(key, SignatureAlgorithm.HS512);
        return build;
    }
}

