package com.wdful.repository;

import com.wdful.base.BaseRepository;
import com.wdful.domain.SmsRecord;
import com.wdful.domain.UserInfo;
import org.springframework.stereotype.Repository;

/**
 * @author Lee
 * @description 短信
 * @date 2020/9/2 2:33 下午
 **/

@Repository
public interface SmsRecordRepository extends BaseRepository<SmsRecord,Long> {


}
