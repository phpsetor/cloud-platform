package com.wdful.repository;

import com.wdful.base.BaseRepository;
import com.wdful.domain.BaseEntityIncre;
import com.wdful.domain.User;
import com.wdful.domain.UserInfo;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Lee
 * @description 用户表
 * @date 2020/9/2 2:33 下午
 **/

@Repository
public interface UserRepository extends BaseRepository<User,Long> {

    Boolean existsByMobileAndIsDelete(String mobile,Boolean isDelete);

    User findByMobile(String mobile);
    Boolean existsByMobile(String mobile);
}
