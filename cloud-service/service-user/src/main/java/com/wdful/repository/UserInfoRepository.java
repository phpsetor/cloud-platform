package com.wdful.repository;

import com.wdful.base.BaseRepository;
import com.wdful.domain.User;
import com.wdful.domain.UserInfo;
import org.springframework.stereotype.Repository;

/**
 * @author Lee
 * @description 用户info表
 * @date 2020/9/2 2:33 下午
 **/

@Repository
public interface UserInfoRepository extends BaseRepository<UserInfo,Long> {


}
