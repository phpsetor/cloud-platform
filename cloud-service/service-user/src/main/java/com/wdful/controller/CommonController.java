package com.wdful.controller;

import com.alibaba.fastjson.JSON;
import com.wdful.exception.BadRequestException;
import com.wdful.resp.UserRespDTO;
import com.wdful.service.CommonService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/common")
@RestController
@Api(tags = "公用服务")
@RequiredArgsConstructor
public class CommonController {

    private final CommonService commonService;

    /**
     * @author Lee
     * @description token解码
     * @date 2020/9/15 11:44 上午
     **/
    @ApiOperation("token解码")
    @PostMapping("/decodeToken")
    public UserRespDTO decodeToken(@RequestHeader("token") String token) {
        return commonService.decodeToken(token);
    }


}
