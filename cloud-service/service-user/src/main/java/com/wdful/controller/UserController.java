package com.wdful.controller;

import com.wdful.req.UserLoginReqDTO;
import com.wdful.req.UserReqDTO;
import com.wdful.resp.UserRespDTO;
import com.wdful.service.UserService;
import com.wdful.enums.NoticeType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
@Api(tags = "用户服务:账号操作")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    @ApiOperation("用户注册")
    public void register(@RequestBody @Validated UserReqDTO userReqDTO) {
        userService.register(userReqDTO);
    }

    @GetMapping("/sendSMS")
    @ApiOperation("短信发送")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeType", value = "通知类型", paramType = "query", dataType = "String", defaultValue = "SMS_REGISTER"),
            @ApiImplicitParam(name = "phone", value = "通知电话 注册必传 其他场景默认获取用户手机号", paramType = "query", dataType = "String", defaultValue = ""),
    })
    public void sendSMS(@RequestParam("noticeType") NoticeType noticeType,
                        @RequestParam(value = "phone", required = false) String phone,
                        @RequestHeader(value = "userId", required = false) Long userId) {
        userService.sendSMS(noticeType, phone, userId);
    }

    @ApiOperation("用户登陆")
    @PostMapping("/userLogin")
    public UserRespDTO userLogin(@RequestBody @Validated UserLoginReqDTO loginReqDTO) {
        return userService.userLogin(loginReqDTO);
    }
}
