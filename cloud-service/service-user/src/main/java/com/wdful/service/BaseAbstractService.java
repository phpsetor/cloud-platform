package com.wdful.service;

import com.wdful.bean.BaseConstant;
import com.wdful.repository.UserRepository;
import com.wdful.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseAbstractService {

    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected RedisUtils redisUtils;
    @Autowired
    protected BaseConstant baseConstant;

    protected Boolean isRepeat(String mobile) {
        return userRepository.existsByMobileAndIsDelete(mobile, Boolean.FALSE);
    }
}
