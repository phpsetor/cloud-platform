package com.wdful.service;

import com.alibaba.fastjson.JSON;
import com.wdful.exception.BadRequestException;
import com.wdful.resp.UserRespDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommonService {

    private final JwtParser jwtParser;

    /**
     * @author Lee
     * @description token解码
     * @date 2020/9/15 11:49 上午
     **/
    public UserRespDTO decodeToken(String token) {
        try {
            final Claims claims = jwtParser.parseClaimsJws(token)
                    .getBody();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date expiration = claims.getExpiration();
            log.info("======== token的过期时间：" + df.format(expiration));
            final Timestamp expireTime = Timestamp.from(expiration.toInstant());
            Timestamp nowTime = Timestamp.from(Instant.now());
            if (nowTime.after(expireTime)) {
                throw new BadRequestException("登陆过期");
            }
            final UserRespDTO userRespDTO = JSON.parseObject(claims.getSubject(), UserRespDTO.class);
            return userRespDTO;
        } catch (BadRequestException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("权限错误");
        }
    }

}
