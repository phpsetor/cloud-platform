package com.wdful.service;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.wdful.domain.SmsRecord;
import com.wdful.domain.User;
import com.wdful.domain.UserInfo;
import com.wdful.enums.NoticeType;
import com.wdful.exception.BadRequestException;
import com.wdful.mapper.UserRespDTOMapper;
import com.wdful.repository.SmsRecordRepository;
import com.wdful.req.UserLoginReqDTO;
import com.wdful.req.UserReqDTO;
import com.wdful.resp.UserRespDTO;
import com.wdful.utils.AssertUtils;
import com.wdful.utils.RsaUtils;
import com.wdful.utils.SnowflakeIdWorker;
import com.wdful.utils.StringUtils;
import io.jsonwebtoken.JwtBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;


@Service
@RequiredArgsConstructor
@Slf4j
public class UserService extends BaseAbstractService {
    @Value("${rsa.private_key}")
    private String privateKey;
    private final JwtBuilder jwtBuilder;
    private final SmsRecordRepository smsRecordRepository;
    private final UserRespDTOMapper userRespDTOMapper;
    private final HttpServletRequest httpServletRequest;

    /**
     * @author Lee
     * @description 用户注册
     * @date 2020/9/10 9:41 上午
     **/
    @Transactional(rollbackFor = Exception.class)
    public void register(UserReqDTO reqDto) {
        final Object code = redisUtils.get(NoticeType.SMS_REGISTER.getRedisKey() + reqDto.getMobile());
        log.debug("获取验证码:{},reids验证码:{}", reqDto.getVerifyCode(), code);
        AssertUtils.notNull(code, "请重新发送验证码");
        AssertUtils.isEqual(code, reqDto.getVerifyCode(), "验证码不正确");
        Boolean isRepeat = super.isRepeat(reqDto.getMobile());
        AssertUtils.isTrue(!isRepeat, "手机号重复");
        User user = new User();
        user.setPassword(reqDto.getPassword());
        user.setMobile(reqDto.getMobile());
        //初始化个人信息
        UserInfo userInfo = new UserInfo();
        userInfo.setNickName("用户" + SnowflakeIdWorker.uniqueSequenceStr().substring(0, 6));
        userInfo.setProfile("用户很懒,没有设置个人简介。");
        userInfo.setHeadImg("");//默认头像
        userInfo.setLevel(0);
        userInfo.setUser(user);
        user.setUserInfo(userInfo);
        userRepository.save(user);
        redisUtils.del(NoticeType.SMS_REGISTER.getRedisKey() + reqDto.getMobile());
    }

    /**
     * @author Lee
     * @description 发送验证码
     * @date 2020/9/12 2:50 下午
     **/
    @Transactional(rollbackFor = Exception.class)
    public void sendSMS(NoticeType noticeType, String phone, Long userId) {
        AssertUtils.notBlank(phone, "手机号不能为空");
        final Object code = redisUtils.get(NoticeType.SMS_REGISTER.getRedisKey() + phone);
        log.info("验证码有效:{}", code);
        if (Objects.nonNull(code)) {
            return;
        }
        String verifyCode = String
                .valueOf(new Random().nextInt(899999) + 100000);
        //todo 发送短信 注册短信需要手机号  其他短信从当前用户获取
        if (Objects.equals(NoticeType.SMS_REGISTER, noticeType)) {
            //检查用户是否存在
            final Boolean aBoolean = userRepository.existsByMobile(phone);
            if (aBoolean) {
                throw new BadRequestException("用户已存在 无需注册");
            }
        } else {
            if (Objects.isNull(userId)) {
                throw new BadRequestException("用户权限错误");
            }
            final Optional<User> userOp = userRepository.findById(userId);
            if (!userOp.isPresent()) {
                throw new BadRequestException("用户不存在");
            }
            phone = userOp.get().getMobile();
        }
        log.info("-------------------->发送的验证码为:{} <--------------------", verifyCode);
        redisUtils.set(NoticeType.SMS_REGISTER.getRedisKey() + phone, verifyCode);
        redisUtils.expire(NoticeType.SMS_REGISTER.getRedisKey() + phone, 1800);
        //记录短信
        SmsRecord smsRecord = new SmsRecord();
        smsRecord.setMobile(phone);
        smsRecord.setResp("");
        smsRecord.setNoticeType(noticeType);
        smsRecord.setResvId(0L);
        smsRecord.setContent(verifyCode);
        smsRecordRepository.save(smsRecord);
    }


    /**
     * @author Lee
     * @description 用户账号密码
     * @date 2020/9/10 9:42 上午
     **/
    @Transactional(rollbackFor = Exception.class)
    public UserRespDTO userLogin(UserLoginReqDTO reqDTO) {
        final Boolean aBoolean = userRepository.existsByMobile(reqDTO.getMobile());
        AssertUtils.isTrue(aBoolean, "用户不存在");
        final User user = userRepository.findByMobile(reqDTO.getMobile());
        // 密码解密
        String password = RsaUtils.decryptByPrivateKey(privateKey, reqDTO.getPasswrod());
        String pwd = RsaUtils.decryptByPrivateKey(privateKey, user.getPassword());
        log.info("传入的密码:{},用户的密码:{}", password, pwd);
        AssertUtils.isEqual(password, pwd, "密码错误");
        user.setLastLoginTime(LocalDateTime.now());
        user.setLastLoginIp(StringUtils.getIp(httpServletRequest));
        user.setLoginAcc(user.getLoginAcc() + 1);
        userRepository.save(user);
        final UserRespDTO userRespDTO = userRespDTOMapper.toDto(user);
        String token = jwtBuilder.setId(IdUtil.simpleUUID())
                .claim("auth", "")
                .setSubject(JSON.toJSONString(userRespDTO))
                .compact();
        userRespDTO.setToken(token);
        return userRespDTO;
    }


}
