package com.wdful.mapper;

import com.wdful.bean.BaseMapper;
import com.wdful.domain.User;
import com.wdful.domain.UserInfo;
import com.wdful.resp.UserInfoRespDTO;
import com.wdful.resp.UserRespDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserInfoRespDTOMapper extends BaseMapper<UserInfoRespDTO,UserInfo> {
}
