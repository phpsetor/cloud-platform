package com.wdful.mapper;

import com.wdful.bean.BaseMapper;
import com.wdful.domain.User;
import com.wdful.resp.UserRespDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE,uses = {UserInfoRespDTOMapper.class})
public interface UserRespDTOMapper extends BaseMapper<UserRespDTO,User> {

    UserRespDTO toDto(User user);
}