package com.wdful.feign;


import com.wdful.feign.fallback.CatServiceFeignClientFallbackFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-user", fallbackFactory = CatServiceFeignClientFallbackFactory.class)
@Qualifier("catServiceFeignClient")
public interface CatServiceFeignClient {

    @GetMapping("/cat/test/{value}")
    String test(@PathVariable("value") String value);
}