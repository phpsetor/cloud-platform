package com.wdful.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Lee
 * @description 分布式事务用表
 * @date 2020/8/28 11:58 上午
 **/
@Entity
@Data
@Table(name = "undo_log")
@NoArgsConstructor
public class SeataUndoLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "BIGINT(20) comment 'id'")
    private Long id;

    @Column(name = "branch_id", columnDefinition = "BIGINT(20) comment '分支id'")
    private Long branchId;

    @Column(name = "xid", columnDefinition = "BIGINT(100) comment '事务id'")
    private String xid;

    @Column(name = "context", columnDefinition = "VARCHAR(128) comment '内容'")
    private String context;

    @Column(name = "rollback_info", columnDefinition = "longblob comment '回滚信息'")
    private Byte[] rollbackInfo;

    @Column(name = "log_status", columnDefinition = "int(11) comment '日志状态'")
    private Integer logStatus;

    @Column(name = "log_created", columnDefinition = "DATETIME comment '创建时间'")
    private LocalDateTime logCreate;

    @Column(name = "log_modified", columnDefinition = "DATETIME comment '修改时间'")
    private LocalDateTime logModified;

    @Column(name = "ext", columnDefinition = "VARCHAR(100) comment '扩展信息'")
    private String ext;

}
