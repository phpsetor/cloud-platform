package com.wdful.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
public class BaseEntityIncre implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "BIGINT(20) comment '主键id'")
    private Long id;

    @CreationTimestamp
    @Column(name = "create_time", updatable = false, columnDefinition = "DATETIME comment '创建时间'")
    @ApiModelProperty(value = "创建时间", hidden = true)
    private LocalDateTime createTime;

    @UpdateTimestamp
    @Column(name = "update_time", columnDefinition = "DATETIME comment '更新时间'")
    @ApiModelProperty(value = "更新时间", hidden = true)
    private LocalDateTime updateTime;

    @Column(name = "is_delete", nullable = false, columnDefinition = "bit default 0 comment '是否删除'")
    @JsonIgnore
    private Boolean isDelete = false;
}