package com.wdful.utils;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.hibernate.HibernateException;
import org.hibernate.transform.ResultTransformer;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.NumberUtils;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Lee
 * @description 结果转化工具
 * @date 2020-01-16 10:03
 **/

public class IgnoreTypeResultTransformer implements ResultTransformer {


    private static final long serialVersionUID = -3779317531110592988L;

    public static ConcurrentMap<String, BeanMap> beanMapCache = new ConcurrentHashMap<String, BeanMap>();
    private final Class<?> resultClass;

    public IgnoreTypeResultTransformer(final Class<?> resultClass) {
        this.resultClass = resultClass;
    }

    private BeanMap getBeanMap(Object object) {
        return BeanMap.create(object);
    }

    private Object transforObject(Object src, Class dest) throws ClassCastException, IllegalArgumentException {
        Object clo = null;
//        String arrayStr = null;
        if (dest.equals(String.class)) {
            clo = JSON.toJSONString(src);
        } else if (dest.getSuperclass().equals(Number.class) || dest.equals(Number.class)) {
            // 只处理 字符转入 或 数值类型转入
            clo = NumberUtils.parseNumber(src.toString(), dest);
        } else if (dest.getSuperclass().equals(Instant.class) || dest.equals(Instant.class)) {
            Timestamp tp = (Timestamp) src;
            clo = tp.toInstant();
        }else if (dest.getSuperclass().equals(LocalDate.class) || dest.equals(LocalDate.class)) {
            Date tp = (Date) src;
            clo =  tp.toInstant();
        }  else {
            //error 设置 null
            System.out.println("未转化值src:{"+src+"}, dest:{"+dest+"}");
        }
        return clo;
    }

    /**
     * aliases为每条记录的数据库字段名,
     * tupe为与aliases对应的字段的值
     */
    @Override
    public Object transformTuple(Object[] tuple, String[] aliases) {
        Object result;
        try {
            result = resultClass.newInstance();
            BeanMap beanMap = getBeanMap(result);
            for (int i = 0, len = tuple.length; i < len; i++) {
                String key = BeanNaming.underLineToCamel(aliases[i]);
                Class target = beanMap.getPropertyType(key);
                if (target == null) {
                    continue;
                }
                Object src = tuple[i];
                if (src != null && !src.getClass().equals(target)) {
                    //存在需要转化的的值。。
                    src = transforObject(src, target);
                }
                beanMap.put(key, src);
            }

        } catch (Exception e) {
            throw new HibernateException("Could not instantiate resultclass: " + this.resultClass.getName(), e);
        }
        return result;
    }

    @Override
    public List transformList(List collection) {
        return collection;
    }

    private static class BeanNaming extends PropertyNamingStrategy.SnakeCaseStrategy {
        private static final char UNDERLINE = '_';
        public static Map<String, String> CACHING = new ConcurrentHashMap<>();

        protected static String underLineToCamel(String param) {
            if (StringUtils.isBlank(param)) {
                return "";
            }
            if (param.indexOf(UNDERLINE) == -1) {
                //不需要转化
                return param;
            }
            if (CACHING.containsKey(param)) {
                return CACHING.get(param);
            }
            int len = param.length();
            StringBuilder sb = new StringBuilder(len);
            for (int i = 0; i < len; i++) {
                char c = param.charAt(i);
                if (c == UNDERLINE) {
                    if (++i < len) {
                        sb.append(Character.toUpperCase(param.charAt(i)));
                    }
                } else {
                    sb.append(c);
                }
            }
            CACHING.put(param, sb.toString());
            return sb.toString();
        }
    }
}
