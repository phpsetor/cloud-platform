package com.wdful.auth;

import com.wdful.enums.Authenticate;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @description 访问地址控制
 * @date 2020/9/15 3:05 下午
 **/
@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "auth.urls")
public class AuthUrl {

    private List<String> skipUrls;

    private Map<String, ArrayList<String>> limitUrls;

}
