package com.wdful.feign;

import com.wdful.feign.fallback.TokenFeignClientFallbackFactory;
import com.wdful.resp.UserRespDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "service-user", fallbackFactory = TokenFeignClientFallbackFactory.class)
@Qualifier("tokenFeignClient")
public interface TokenFeignClient {

    @PostMapping("/common/decodeToken")
    UserRespDTO decodeToken(@RequestHeader("token") String token);
}
