package com.wdful.feign.fallback;

import com.wdful.exception.BadRequestException;
import com.wdful.feign.TokenFeignClient;
import com.wdful.resp.UserRespDTO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Slf4j
@Component
public class TokenFeignClientFallbackFactory implements FallbackFactory<TokenFeignClient> {
    @Override
    public TokenFeignClient create(Throwable throwable) {
        log.info("throwable is {}",throwable);
        if(Objects.nonNull(throwable) && Objects.nonNull(throwable.getCause())){
            throw new BadRequestException(throwable.getCause().getMessage());
        }
        return new TokenFeignClient() {

            @Override
            public UserRespDTO decodeToken(String token) {
                return null;
            }
        };
    }
}