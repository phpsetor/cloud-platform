package com.wdful.router;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Lee
 * @description 路由管理
 * @date 2020/9/1 9:51 上午
 **/
@Configuration
public class ManageRouter {

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("path2ManageService",
                r -> r.path("/manageApi/**")
                        .uri("lb://cloud-manage"))
                .route("path2UserService",
                        r -> r.path("/restApi/**")
                                .uri("lb://cloud-rest"))
                .build();
        return routes.build();
    }
}
