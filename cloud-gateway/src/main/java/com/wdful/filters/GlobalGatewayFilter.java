package com.wdful.filters;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wdful.auth.AuthUrl;
import com.wdful.exception.BadFeignRequestException;
import com.wdful.exception.BadRequestException;
import com.wdful.feign.TokenFeignClient;
import com.wdful.resp.UserRespDTO;
import com.wdful.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.NettyWriteResponseFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Lee
 * @description 处理跨域响应头以及权限
 * @date 2020/9/15 3:30 下午
 **/
@Component
@Setter
@Getter
@Slf4j
public class GlobalGatewayFilter implements GlobalFilter, Ordered {

    @Autowired
    private AuthUrl authUrl;

    private ObjectMapper objectMapper;

    private TokenFeignClient tokenFeignClient;

    public GlobalGatewayFilter(ObjectMapper objectMapper, TokenFeignClient tokenFeignClient) {
        this.objectMapper = objectMapper;
        this.tokenFeignClient = tokenFeignClient;
    }

    @Override
    public int getOrder() {
        // 指定此过滤器位于NettyWriteResponseFilter之后
        // 即待处理完响应体后接着处理响应头
        return NettyWriteResponseFilter.WRITE_RESPONSE_FILTER_ORDER + 1;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String url = exchange.getRequest().getURI().getPath();
        log.info("当前URL:{},不需要校验的URL:{}", url, authUrl.getSkipUrls());
        //跳过不需要验证的路径
        if (CollectionUtils.isNotEmpty(authUrl.getSkipUrls()) && authUrl.getSkipUrls().stream().anyMatch(s -> url.contains(s))) {
            return handlerResponse(exchange, chain);
        }
        //获取token
        String token = exchange.getRequest().getHeaders().getFirst("Authorization");
        ServerHttpResponse resp = exchange.getResponse();
        if (StringUtils.isBlank(token)) {
            //没有token
            return authErro(resp, "请登陆");
        } else {
            //有token
            UserRespDTO userRespDTO = null;
            try {
                userRespDTO = tokenFeignClient.decodeToken(token);
            } catch (Exception e) {
                throw JSON.parseObject(e.getMessage(), BadRequestException.class);
            }
            if (Objects.nonNull(userRespDTO) && Objects.nonNull(userRespDTO.getId())) {
                if (authUrl.getLimitUrls().containsKey(userRespDTO.getAuthenticate().toString())) {
                    final ArrayList<String> limitUrls = authUrl.getLimitUrls().get(userRespDTO.getAuthenticate().toString());
                    log.info("权限:{},限制访问的URL:{}", userRespDTO.getAuthenticate(), limitUrls);
                    if (limitUrls.stream().anyMatch(s -> url.contains(s))) {
                        return authErro(resp, "权限不足");
                    }
                }
                return handlerResponse(exchange, chain);
            } else {
                return authErro(resp, "登陆已经过期");
            }
        }
    }

    /**
     * @author Lee
     * @description 处理响应头
     * @date 2020/9/15 3:34 下午
     **/
    private Mono<Void> handlerResponse(ServerWebExchange exchange, GatewayFilterChain chain) {
        return chain.filter(exchange).then(Mono.defer(() -> {
            exchange.getResponse().getHeaders().entrySet().stream()
                    .filter(kv -> (kv.getValue() != null && kv.getValue().size() > 1))
                    .filter(kv -> (kv.getKey().equals(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN)
                            || kv.getKey().equals(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS)))
                    .forEach(kv -> kv.setValue(new ArrayList<String>() {{
                        add(kv.getValue().get(0));
                    }}));
            return chain.filter(exchange);
        }));
    }

    /**
     * 认证错误输出
     *
     * @param resp 响应对象
     * @param mess 错误信息
     * @return
     */
    private Mono<Void> authErro(ServerHttpResponse resp, String mess) {
        resp.setStatusCode(HttpStatus.UNAUTHORIZED);
        resp.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        ApiError apiError = ApiError.error(HttpStatus.UNAUTHORIZED.value(), mess);
        String returnStr = "";
        try {
            returnStr = objectMapper.writeValueAsString(apiError);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
        DataBuffer buffer = resp.bufferFactory().wrap(returnStr.getBytes(StandardCharsets.UTF_8));
        return resp.writeWith(Flux.just(buffer));
    }

}