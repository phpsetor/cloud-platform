package com.wdful.feign.fallback;

import com.wdful.exception.BadRequestException;
import com.wdful.feign.CatServiceFeignClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CatServiceFeignClientFallbackFactory implements FallbackFactory<CatServiceFeignClient> {
    @Override
    public CatServiceFeignClient create(Throwable throwable) {
        return new CatServiceFeignClient() {
            @Override
            public String test(String value) {
                log.error("测试，参数=>【{}】", value);
                throw new BadRequestException("网络错误");
            }
        };
    }
}
