package com.wdful.controller;

import com.wdful.annotation.AnonymousAccess;
import com.wdful.annotation.AnonymousGetMapping;
import com.wdful.annotation.AnonymousPostMapping;
import com.wdful.exception.BadRequestException;
import com.wdful.utils.CosUtil;
import com.wdful.utils.FileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "公用接口:上传")
@RestController
@RequiredArgsConstructor
@RequestMapping("/common")
public class CommonController {

    @Autowired
    private CosUtil cosUtil;

    @PostMapping(value = "/upload/{type}")
    @ApiOperation("资料上传 1用户文件  2系统文件")
    @AnonymousPostMapping
    public ResponseEntity upload(@PathVariable("type") Integer type,
                                 @RequestPart("file") MultipartFile file) {
        try {
            return ResponseEntity.ok(cosUtil.upload(type, FileUtil.toFile(file)));
        } catch (Exception e) {
            throw new BadRequestException("上传失败");
        }
    }

    @GetMapping(value = "/getTempSecret")
    @ApiOperation("获取临时上传秘钥")
    @AnonymousGetMapping
    public ResponseEntity getTempSecret() {
            return ResponseEntity.ok(cosUtil.getTempSecret());
    }
}
