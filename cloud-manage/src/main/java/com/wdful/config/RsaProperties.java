package com.wdful.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lee
 * @description
 * @date 2020-05-18
 **/
@Data
@Component
public class RsaProperties {
    
    @Value("${rsa.private_key}")
    public String privateKey;

}