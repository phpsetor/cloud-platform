package com.wdful.utils;

import com.alibaba.fastjson.JSON;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.region.Region;
import com.tencent.cloud.CosStsClient;
import com.wdful.exception.BadRequestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.wdful.resp.Credentials;

import java.io.File;
import java.util.TreeMap;

@Component
public class CosUtil {


    @Value("${tencent.cos.secretId}")
    private String secretId;

    @Value("${tencent.cos.secretKey}")
    private String secretKey;

    @Value("${tencent.cos.regionUrl}")
    private String regionUrl;

    @Value("${tencent.cos.bucket}")
    private String bucket;

    /**
     * @author Lee
     * @description 文件上传
     * @date 2020/9/9 下午8:08
     **/
    public String upload(int type ,File localFile) {
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        Region region = new Region(regionUrl);
        ClientConfig clientConfig = new ClientConfig(region);
        COSClient cosClient = new COSClient(cred, clientConfig);
        String key = 1 == type?"user/":"sys/";
        try {
            //文件原始名称
            String originalFilename = localFile.getName();
            final String format = originalFilename.substring(originalFilename.lastIndexOf("."));
            key = key+SnowflakeIdWorker.uniqueSequenceStr() + format;
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key, localFile);
            cosClient.putObject(putObjectRequest);
            return String.format("https://%s.cos.%s.myqcloud.com/%s",bucket,regionUrl,key);
        } catch (Exception e) {
            throw new BadRequestException("上传失败");
        } finally {
            cosClient.shutdown();
        }
    }

    /**
     * @author Lee
     * @description 获取临时秘钥
     * @date 2020/9/9 下午8:08
     **/
    public Credentials getTempSecret(){
        TreeMap<String, Object> config = new TreeMap<String, Object>();
        try {
            // 替换为您的 SecretId
            config.put("SecretId", secretId);
            // 替换为您的 SecretKey
            config.put("SecretKey", secretKey);
            // 临时密钥有效时长，单位是秒，默认1800秒，目前主账号最长2小时（即7200秒），子账号最长36小时（即129600秒）
            config.put("durationSeconds", 1800);
            // 换成您的 bucket
            config.put("bucket", bucket);
            // 换成 bucket 所在地区
            config.put("region", regionUrl);
            // 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的具体路径，例子：a.jpg 或者 a/* 或者 * 。
            // 如果填写了“*”，将允许用户访问所有资源；除非业务需要，否则请按照最小权限原则授予用户相应的访问权限范围。
            config.put("allowPrefix", "/user");
            // 密钥的权限列表。简单上传、表单上传和分片上传需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
            String[] allowActions = new String[] {
                    // 简单上传
                    "name/cos:PutObject",
                    // 表单上传、小程序上传
                    "name/cos:PostObject",
                    // 分片上传
                    "name/cos:InitiateMultipartUpload",
                    "name/cos:ListMultipartUploads",
                    "name/cos:ListParts",
                    "name/cos:UploadPart",
                    "name/cos:CompleteMultipartUpload"
            };
            config.put("allowActions", allowActions);

            JSONObject credential = CosStsClient.getCredential(config);
            //成功返回临时密钥信息，如下打印密钥信息
            return JSON.parseObject(credential.toString(),Credentials.class);
        } catch (Exception e) {
            //失败抛出异常
            throw new IllegalArgumentException("no valid secret!");
        }
    }
}
