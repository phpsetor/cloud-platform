package com.wdful.mapper;

import com.wdful.bean.BaseMapper;
import com.wdful.domain.ApiLog;
import com.wdful.dto.LogSmallDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author lee
 * @date 2019-5-22
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LogSmallMapper extends BaseMapper<LogSmallDTO, ApiLog> {

}