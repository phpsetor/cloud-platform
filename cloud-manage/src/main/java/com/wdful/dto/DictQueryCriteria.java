package com.wdful.dto;

import lombok.Data;
import com.wdful.annotation.Query;

/**
 * @author lee
 * 公共查询类
 */
@Data
public class DictQueryCriteria {

    @Query(blurry = "name,description")
    private String blurry;
}
