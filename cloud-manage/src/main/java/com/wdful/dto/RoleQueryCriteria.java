package com.wdful.dto;

import lombok.Data;
import com.wdful.annotation.Query;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author lee
 * 公共查询类
 */
@Data
public class RoleQueryCriteria {

    @Query(blurry = "name,description")
    private String blurry;

    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> createTime;
}
