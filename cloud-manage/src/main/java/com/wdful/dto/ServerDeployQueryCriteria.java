package com.wdful.dto;

import com.wdful.annotation.Query;
import lombok.Data;
import java.sql.Timestamp;
import java.util.List;

/**
* @author lee
* @date 2019-08-24
*/
@Data
public class ServerDeployQueryCriteria{

	/**
	 * 模糊
	 */
	@Query(blurry = "name,ip,account")
    private String blurry;

	@Query(type = Query.Type.BETWEEN)
	private List<Timestamp> createTime;
}
