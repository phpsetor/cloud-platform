package com.wdful.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
* @author lee
* @date 2019-04-10
*/
@Getter
@Setter
public class DictSmallDto implements Serializable {

    private Long id;
}
