package com.wdful.config;

import feign.Feign;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import feign.Retryer;
import feign.querymap.BeanQueryMapEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class FeignClientRequestConfig implements RequestInterceptor {


    @Override
    public void apply(RequestTemplate requestTemplate) {
//        log.info("feign客户端调用加入参数:token->【{}】", );
//        requestTemplate.query("token", token);
    }

    /**
     * @author Lee
     * @description 替换解析queryMap的类，实现父类中变量的映射
     * @date 2020/7/2 5:39 下午
     **/
    @Bean
    public Feign.Builder feignBuilder() {
        return Feign.builder()
                .queryMapEncoder(new BeanQueryMapEncoder())
                .retryer(Retryer.NEVER_RETRY);
    }

}
