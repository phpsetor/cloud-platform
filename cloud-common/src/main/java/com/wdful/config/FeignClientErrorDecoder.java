package com.wdful.config;

import com.wdful.exception.BadFeignRequestException;
import com.wdful.exception.BadRequestException;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

import static feign.FeignException.errorStatus;

@Configuration
@Slf4j
public class FeignClientErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        String body = null;
        try {
            body = Util.toString(response.body().asReader());
        } catch (IOException e) {
        }
        if (response.status() >= 400 && response.status() <= 500) {
            throw new BadFeignRequestException(body);
        }
        return errorStatus(methodKey, response);
    }
}
