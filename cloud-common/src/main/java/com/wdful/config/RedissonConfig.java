package com.wdful.config;

import java.io.IOException;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 *  @author: hanhan
 *  @Date: 2020/9/17 10:19 上午
 *  @param
 *  @return
 *  @Description:
 */
@Configuration
@EnableConfigurationProperties(RedisConfigProperties.class)
public class RedissonConfig {

    /**
     * 所有对Redisson的使用都是通过RedissonClient
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod="shutdown")
    public RedissonClient redisson(RedisConfigProperties properties) throws IOException {
        //1、创建配置
        Config config = new Config();
        String url = "redis://" + properties.getHost() + ":" + properties.getPort();
        config.useSingleServer().setAddress(url);
        config.useSingleServer().setDatabase(properties.getDatabase());
        //2、根据Config创建出RedissonClient实例
        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;
    }

}
