package com.wdful.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: hanhan
 * @version: V1.0
 * date: 2020-09-17 10:03
 */
@ConfigurationProperties(prefix = "spring.redis")
@Data
public class RedisConfigProperties {
    private Integer database;
    private String host;
    private String port;
    private String password;
}
