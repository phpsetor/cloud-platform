package com.wdful.bean;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Data
@Component
@RefreshScope
@Accessors(fluent = true)
public class BaseConstant {


    @Value("${spring.application.name}")
    private String appName;

    //幂等性redis key
    private String idempotentKey = "request:idempotent";



}

