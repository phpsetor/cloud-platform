package com.wdful.utils;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页工具
 *
 * @author lee
 * @date 2018-12-10
 */
public class PageUtil extends cn.hutool.core.util.PageUtil {

    /**
     * List 分页
     */
    public static List toPage(int page, int size , List list) {
        int fromIndex = page * size;
        int toIndex = page * size + size;
        if (fromIndex > list.size()) {
            return new ArrayList();
        } else if (toIndex >= list.size()) {
            return list.subList(fromIndex, list.size());
        } else {
            return list.subList(fromIndex, toIndex);
        }
    }


    /**
     * Page 数据处理，预防redis反序列化报错
     */
    public static Map<String, Object> toPage(Page page) {
        Map<String, Object> map = new LinkedHashMap<>(4);
        map.put("content", page.getContent());
        map.put("page", page.getNumber());
        map.put("size", page.getSize());
        map.put("totalElements", page.getTotalElements());
        return map;
    }

    /**
     * 自定义分页
     */
    public static Map<String, Object> toPage(Object object, int page, int size, Object totalElements) {
        Map<String, Object> map = new LinkedHashMap<>(4);
        map.put("content", object);
        map.put("page", page);
        map.put("size", size);
        map.put("totalElements", totalElements);
        return map;
    }

    public static Map<String, Object> toPage(Object object, Object totalElements) {
        Map<String, Object> map = new LinkedHashMap<>(4);
        map.put("content", object);
        map.put("totalElements", totalElements);
        return map;
    }


    /**
     * @author Lee
     * @description 无需分页的返回
     * @date 2020/7/7 3:22 下午
     **/
    public static Map<String, Object> toList(List list) {
        Map<String, Object> map = new LinkedHashMap<>(4);
        map.put("content", list);
        map.put("page", 0);
        map.put("size", list.size());
        map.put("totalElements", list.size());
        return map;
    }


    public static Map<String, Object> toList(int page, int size, List list) {
        Map<String, Object> map = new LinkedHashMap<>(4);
        map.put("content", list);
        map.put("page", page);
        map.put("size", size);
        map.put("totalElements", list.size());
        return map;
    }

}
