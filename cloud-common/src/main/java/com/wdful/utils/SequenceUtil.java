package com.wdful.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.SECONDS;

@Component
public class SequenceUtil {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    private final static String PREFIX_ORDER = "DD";
    private final static String PREFIX_PAY_ORDER = "ZF";
    private final static String PREFIX_REFUND_ORDER = "TK";
    private final static String PREFIX_MAINTAIN_ORDER = "BY";
    private final static String PREFIX_REPAIR_ORDER = "RE";
    private final static String PREFIX_RENEW_ORDER = "RN";
    private final static String PREFIX_BREAKRULE_ORDER = "BR";

    static final int DEFAULT_LENGTH = 4;

    private String getIncrementNum(String key) {
        RedisAtomicLong entityIdCounter = new RedisAtomicLong(key, redisTemplate.getConnectionFactory());
        Long counter = entityIdCounter.incrementAndGet();
        LocalDateTime limitTime = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        final long between = SECONDS.between(LocalDateTime.now(), limitTime);
        entityIdCounter.expire(between, TimeUnit.SECONDS);
        return getSequence(counter);
    }

    private String getSequence(long seq) {
        String str = String.valueOf(seq);
        int len = str.length();
        if (len >= DEFAULT_LENGTH) {// 取决于业务规模,应该不会到达3
            return str;
        }
        int rest = DEFAULT_LENGTH - len;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rest; i++) {
            sb.append('0');
        }
        sb.append(str);
        return sb.toString();
    }

    private String currentDate() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    public String getNextOrderNo() {
        return PREFIX_ORDER.concat(currentDate()).concat(getIncrementNum(PREFIX_ORDER));
    }

    public String getNextReNewOrderNo() {
        return PREFIX_RENEW_ORDER.concat(currentDate()).concat(getIncrementNum(PREFIX_RENEW_ORDER));
    }

    public String getNextRepairNO() {
        return PREFIX_REPAIR_ORDER.concat(currentDate()).concat(getIncrementNum(PREFIX_REPAIR_ORDER));
    }

    public String getNextMaintainNO() {
        return PREFIX_MAINTAIN_ORDER.concat(currentDate()).concat(getIncrementNum(PREFIX_MAINTAIN_ORDER));
    }

    public String getNextBreakruleOrderNO() {
        return PREFIX_BREAKRULE_ORDER.concat(currentDate()).concat(getIncrementNum(PREFIX_BREAKRULE_ORDER));
    }

    public String getNextPayOrderNO() {
        return PREFIX_PAY_ORDER.concat(currentDate()).concat(getIncrementNum(PREFIX_PAY_ORDER));
    }

    public String getNextRefundNO() {
        return PREFIX_REFUND_ORDER.concat(currentDate()).concat(getIncrementNum(PREFIX_REFUND_ORDER));
    }


}

