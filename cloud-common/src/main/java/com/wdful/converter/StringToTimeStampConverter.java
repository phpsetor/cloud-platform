package com.wdful.converter;

import com.wdful.utils.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Lee
 * @description 定义一个常用时间转换器
 * @date 2020/4/16 9:28
 **/
public class StringToTimeStampConverter implements Converter<String, Timestamp> {
    private static final String dateHourFormat = "yyyy-MM-dd HH:mm";
    private static final String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    private static final String shortDateFormat = "yyyy-MM-dd";

    @Override
    public Timestamp convert(String source) {
        if (StringUtils.isBlank(source)) {
            return null;
        }
        source = source.trim();
        try {
            DateTimeFormatter formatter;
            if (source.contains("-")) {
                if (source.contains(":")) {
                    if (source.length() == 16) {
                        formatter = DateTimeFormatter.ofPattern(dateHourFormat);
                    } else {
                        formatter = DateTimeFormatter.ofPattern(dateTimeFormat);
                    }
                } else {
                    formatter = DateTimeFormatter.ofPattern(shortDateFormat);
                }
                return Timestamp.valueOf(LocalDateTime.parse(source, formatter));
            } else {
                return Timestamp.valueOf(source);
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("parser %s to Date fail", source));
        }
    }
}
