package com.wdful.aspect;


import com.wdful.annotation.ApiIdempotent;
import com.wdful.bean.BaseConstant;
import com.wdful.exception.BadRequestException;
import com.wdful.utils.RedisUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author Lee
 * @description 冪等性注解切面
 * @date 2020/4/13 15:11
 **/
@Aspect
@Component
public class ApiIdempotentAspect {
    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private BaseConstant baseConstant;

    @Pointcut("@annotation(com.wdful.annotation.ApiIdempotent)")
    public void ApiIdempotent() {
    }


    // 环绕通知验证参数
    @Around("ApiIdempotent()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        ApiIdempotent apiIdempotent = signature.getMethod().getDeclaredAnnotation(ApiIdempotent.class);
        if (apiIdempotent != null) {
            return apiIdempotent(proceedingJoinPoint, signature);
        }
        // 放行
        Object proceed = proceedingJoinPoint.proceed();
        return proceed;
    }

    // 验证Token
    public Object apiIdempotent(ProceedingJoinPoint proceedingJoinPoint, MethodSignature signature)
            throws Throwable {
        ApiIdempotent apiIdempotent = signature.getMethod().getDeclaredAnnotation(ApiIdempotent.class);
        if (apiIdempotent == null) {
            // 直接执行程序
            Object proceed = proceedingJoinPoint.proceed();
            return proceed;
        }
        // 代码步骤：
        // 1.获取令牌 存放在请求头中
        HttpServletRequest request = getRequest();
        String valueType = apiIdempotent.type();
        if (StringUtils.isEmpty(valueType)) {
            throw new BadRequestException("注解参数错误!");
        }
        String token = null;
        if (valueType.equals("head")) {
            token = request.getHeader("token");
        } else {
            token = request.getParameter("token");
        }
        if (StringUtils.isEmpty(token)) {
            throw new BadRequestException("缺少幂等性校验token!");
        }
        if (Objects.isNull(redisUtils.get(baseConstant.idempotentKey().concat(token)))) {
            throw new BadRequestException("请勿重复提交!");
        }
        Object proceed = proceedingJoinPoint.proceed();
        redisUtils.del(baseConstant.idempotentKey().concat(token));
        return proceed;
    }

    public HttpServletRequest getRequest() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        return request;
    }
}
