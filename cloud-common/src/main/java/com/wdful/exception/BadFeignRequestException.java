package com.wdful.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * @author lee
 * @date 2018-11-23
 * feign异常处理
 */
@Getter
public class BadFeignRequestException extends RuntimeException{

    private Integer status = BAD_REQUEST.value();

    public BadFeignRequestException(String msg){
        super(msg);
    }

    public BadFeignRequestException(HttpStatus status, String msg){
        super(msg);
        this.status = status.value();
    }
}
