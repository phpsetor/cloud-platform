package com.wdful.controller;

import com.wdful.enums.NoticeType;
import com.wdful.feign.UserServiceFeignClient;
import com.wdful.req.UserLoginReqDTO;
import com.wdful.req.UserReqDTO;
import com.wdful.resp.UserRespDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "用户服务接口")
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserServiceFeignClient userServiceFeignClient;


    @PostMapping("/register")
    @ApiOperation("用户注册")
    public ResponseEntity register(@RequestBody @Validated UserReqDTO userReqDTO) {
        userServiceFeignClient.register(userReqDTO);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/sendSMS")
    @ApiOperation("短信发送")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeType", value = "通知类型", paramType = "query", dataType = "String", defaultValue = "SMS_REGISTER"),
            @ApiImplicitParam(name = "phone", value = "通知电话 注册必传 其他场景默认获取用户手机号", paramType = "query", dataType = "String", defaultValue = ""),
    })
    public ResponseEntity sendSMS(@RequestParam("noticeType") NoticeType noticeType,
                                  @RequestParam(value = "phone", required = false) String phone,
                                  @RequestHeader(value = "userId", required = false) Long userId) {
        userServiceFeignClient.sendSMS(noticeType, phone, userId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation(value = "用户登陆",response = UserRespDTO.class)
    @PostMapping("/userLogin")
    public ResponseEntity userLogin(@RequestBody @Validated UserLoginReqDTO loginReqDTO) {
        return ResponseEntity.ok(userServiceFeignClient.userLogin(loginReqDTO));
    }
}
