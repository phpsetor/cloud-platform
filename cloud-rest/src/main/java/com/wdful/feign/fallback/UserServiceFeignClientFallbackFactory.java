package com.wdful.feign.fallback;

import com.wdful.enums.NoticeType;
import com.wdful.exception.BadRequestException;
import com.wdful.feign.UserServiceFeignClient;
import com.wdful.req.UserLoginReqDTO;
import com.wdful.resp.UserRespDTO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.wdful.req.UserReqDTO;

import java.util.Objects;

@Slf4j
@Component
public class UserServiceFeignClientFallbackFactory implements FallbackFactory<UserServiceFeignClient> {

    @Override
    public UserServiceFeignClient create(Throwable throwable) {
        log.info("throwable is {}",throwable);
        if(Objects.nonNull(throwable) && Objects.nonNull(throwable.getCause())){
            throw new BadRequestException(throwable.getCause().getMessage());
        }
        return new UserServiceFeignClient() {
            @Override
            public void register(UserReqDTO userReqDTO) {
                log.error("UserServiceFeignClient#register 熔断");
            }

            @Override
            public UserRespDTO userLogin(UserLoginReqDTO loginReqDTO) {
                return new UserRespDTO();
            }

            @Override
            public void sendSMS(NoticeType noticeType, String phone, Long userId) {
                log.error("UserServiceFeignClient#sendSMS 熔断");
            }
        };
    }
}
