package com.wdful.feign;

import com.wdful.enums.NoticeType;
import com.wdful.feign.fallback.UserServiceFeignClientFallbackFactory;
import com.wdful.req.UserLoginReqDTO;
import com.wdful.req.UserReqDTO;
import com.wdful.resp.UserRespDTO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "service-user", fallbackFactory = UserServiceFeignClientFallbackFactory.class)
@Qualifier("userServiceFeignClient")
public interface UserServiceFeignClient {

    @ApiOperation("用户注册")
    @PostMapping("/user/register")
     void register(@RequestBody @Validated UserReqDTO userReqDTO);

    @ApiOperation("用户登陆")
    @PostMapping("/user/userLogin")
    UserRespDTO userLogin(@RequestBody @Validated UserLoginReqDTO loginReqDTO);

    @GetMapping("/user/sendSMS")
    @ApiOperation("短信发送")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noticeType", value = "通知类型", paramType = "query", dataType = "String", defaultValue = "SMS_REGISTER"),
            @ApiImplicitParam(name = "phone", value = "通知电话 注册必传 其他场景默认获取用户手机号", paramType = "query", dataType = "String", defaultValue = ""),
    })
    void sendSMS(@RequestParam("noticeType") NoticeType noticeType,
                        @RequestParam(value = "phone", required = false) String phone,
                        @RequestHeader(value = "userId", required = false) Long userId);
}